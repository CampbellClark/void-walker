using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TextMan : MonoBehaviour
{
    public Text text1;
    public Text text2;
    public Text text3;
    public Text text4;

    public Text next;
    // Start is called before the first frame update
    void Start()
    {
        text1.gameObject.SetActive(true);
        text2.gameObject.SetActive(true);
        StartCoroutine(Textf());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }


    public IEnumerator Textf()
    {
        yield return new WaitForSeconds(3);

        text3.gameObject.SetActive(true);
        text4.gameObject.SetActive(true);
        StartCoroutine(nextpage());
        StopCoroutine(Textf());
    }

    public IEnumerator nextpage()
    {
        yield return new WaitForSeconds(5);
        text1.gameObject.SetActive(false);
        text2.gameObject.SetActive(false);
        text3.gameObject.SetActive(false);
        text4.gameObject.SetActive(false);

        next.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        SceneManager.LoadScene("MainMenu");
    }
}
