using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTestTrigger : MonoBehaviour
{
    public GameObject button;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        button.gameObject.SetActive(true);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        button.gameObject.SetActive(false);
    }


}
