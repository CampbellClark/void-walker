using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BossScript : MonoBehaviour
{
    public int health;
    public int damage;
    private float timeBtwDamage = 1.5f;

    public Animator redPanel;
    public Animator camAnim;
    public Slider healthBar;

    // Update is called once per frame
    void Update()
    {
        healthBar.value = health;
    }
}
