using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{

	public Transform player;
	public float speed;

	public bool isFlipped = false;
	public float attackRange = 3f;

	public Animator animator;

	private void Start()
    {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }
    private void Update()
    {
        if(Vector2.Distance(transform.position, player.position) > 3)
        {
			transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
		if (Vector2.Distance(player.position, transform.position) <= attackRange)
		{
			animator.SetTrigger("Attack");
			animator.ResetTrigger("Attack");

		}
	}
    public void LookAtPlayer()
	{
		Vector3 flipped = transform.localScale;
		flipped.z *= -1f;

		if (transform.position.x > player.position.x && isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = false;
		}
		else if (transform.position.x < player.position.x && !isFlipped)
		{
			transform.localScale = flipped;
			transform.Rotate(0f, 180f, 0f);
			isFlipped = true;
		}
	}

}