using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class aiPatrol : MonoBehaviour
{
    public float speed;
    public float range;
    public float shootSpeed;
    private float distToPlayer;
    private bool canShoot = true;
    public bool movingLeft = true;

    public float timeBetweenShots;

    public Animator anim;
    public Transform groundDetection;
    private bool searchingForPlayer = false;

    public Transform player;
    public Transform shootPos;

    public GameObject bullet;
    private void Start()
    {
        anim.SetBool("canShoot", true);
    }
    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }

        transform.Translate(Vector2.left * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, 2f);
        if (groundInfo.collider == false)
        {
            if(movingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingLeft = false;
                anim.SetBool("movingLeft", false);

            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = true;
                anim.SetBool("movingLeft", true);

            }
        }
        distToPlayer = Vector2.Distance(transform.position, player.position);

        if(distToPlayer <= range)
        {
            if (player.position.x > transform.position.x && transform.localScale.x < 0 || player.position.x < transform.position.x && transform.localScale.x > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingLeft = true;
            }
            if (canShoot)
            {
                StartCoroutine(Shoot());

            }
        }
    }
 

  IEnumerator Shoot()
    {
        Vector2 impulse = new Vector2(-20, 0);


        canShoot = false;
        anim.SetBool("canShoot", false);

        speed = 0;
        yield return new WaitForSeconds(timeBetweenShots);
        if (!movingLeft)
        {
            GameObject newBullet = Instantiate(bullet, shootPos.position, Quaternion.identity);
            newBullet.GetComponent<Rigidbody2D>().AddForce(impulse, ForceMode2D.Impulse);
        }
        
        if (movingLeft)
        {
            Vector2 right = new Vector2(20, 0);
            GameObject newBulletr = Instantiate(bullet, shootPos.position, Quaternion.identity);

            newBulletr.GetComponent<Rigidbody2D>().AddForce(right, ForceMode2D.Impulse);
        }
        canShoot = true;
        anim.SetBool("canShoot", true);

        speed = 10;
        
        
    }
    IEnumerator SearchForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            searchingForPlayer = false;
            player = sResult.transform;
            
            yield break;
        }
    }



}
