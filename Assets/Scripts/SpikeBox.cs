using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpikeBox : MonoBehaviour
{
    public Animator anim;
    public bool isShaking;
    int waitForShake = 4;
    public bool isFalling;
    public float cooldown = 5f;
    // Start is called before the first frame update
    void Start()
    {
        isShaking = true;
        anim.SetBool("isShaking", true);
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time < cooldown)
        {
            StartCoroutine(falling());
        }

    }

  



    public IEnumerator falling()
    {
        
        yield return new WaitForSeconds(4);
        isFalling = true;
        anim.SetBool("isFalling", true);
        

        
        anim.SetBool("isShaking", false);
        isShaking = false;
        anim.SetBool("isFalling", false);
        isFalling = false;
    }
}
