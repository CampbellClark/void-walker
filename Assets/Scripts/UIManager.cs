using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{

    public GameObject pauseMenu;

    public PauseMenu pause;
    public int numberOfEsc = 1;
    
    void Update()
    {
        PauseMenu();
    }


    public void PauseMenu()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && numberOfEsc == 1)
        {
            pauseMenu.SetActive(true);
            
            Time.timeScale = 0;
            numberOfEsc = 0;
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && numberOfEsc == 0)
        {
            pauseMenu.SetActive(false);
            
            Time.timeScale = 1;
            numberOfEsc = 1;
        }

        


    }
}
