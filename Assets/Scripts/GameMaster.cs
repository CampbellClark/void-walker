using UnityEngine;
using System.Collections;

public class GameMaster : MonoBehaviour
{

	public static GameMaster gm;

	void Awake()
	{
		if (gm == null)
		{
			gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();
		}
	}

	AudioSource respawn;
	public AudioClip clip;

	public Transform playerPrefab;
	public Transform spawnPoint;
	public int spawnDelay = 2;
	public Transform spawnPrefab;

	public IEnumerator RespawnPlayer()
	{
	
		yield return new WaitForSeconds(spawnDelay);

		Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);
		FindObjectOfType<AudioManager>().Play("Respawn");

		Instantiate(spawnPrefab, spawnPoint.position, spawnPoint.rotation);
	
	}

	public static void KillPlayer(Player player)
	{
		Destroy(player.gameObject);
		gm.StartCoroutine(gm.RespawnPlayer());
	}

	public static void KillEnemy(Enemy enemy)
    {
		Destroy(enemy.gameObject);
    }

	

}