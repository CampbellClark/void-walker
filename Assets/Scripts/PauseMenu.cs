using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public Button resumeButton;
    public Button mainMenuButton;
    public Button quitButton;
    public Transform pauseMenu;
    public GameObject confirm;
    public GameObject optionsMenu;
    public GameObject gamePaused;
    // Options menu


    public void Options()
    {
        mainMenuButton.gameObject.SetActive(false);
        quitButton.gameObject.SetActive(false);

        pauseMenu.gameObject.SetActive(false);
        optionsMenu.SetActive(true);
    }

    public void resumeGame()
    {
        pauseMenu.gameObject.SetActive(false);
        optionsMenu.SetActive(false);
        Time.timeScale = 1;
    }


    public void returnToMenu()
    {
        pauseMenu.gameObject.SetActive(false);

        confirm.gameObject.SetActive(true);

        
    }



    public void confirmMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


    public void returnToPauseMenu()
    {
        pauseMenu.gameObject.SetActive(true);
        quitButton.gameObject.SetActive(true);
        mainMenuButton.gameObject.SetActive(true);
        gamePaused.SetActive(true);
        optionsMenu.SetActive(false);

    }

}