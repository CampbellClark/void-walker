using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeThrower : MonoBehaviour
{
    public float throwForce = 600f;
    public GameObject grenadePrefab;
    PlatformerCharacter2D platformer;
    public GameObject thrower;

    public bool canThrow;
    // Update is called once per frame
    private void Start()
    {
        canThrow = true;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G) && canThrow)
        {
            StartCoroutine(grenadeCooldown());
            ThrowGrenade();
        } 
    }


    void ThrowGrenade()
    {

        Vector2 throwf = new Vector2(500, 0);

        GameObject grenade = Instantiate(grenadePrefab, thrower.transform.position, transform.rotation);
        Rigidbody2D rb = grenade.GetComponent<Rigidbody2D>();
        rb.AddForce(Vector2.right * throwf);


    }

    IEnumerator grenadeCooldown()
    {
        canThrow = false;
        yield return new WaitForSeconds(8);
        canThrow = true;
        StopCoroutine(grenadeCooldown());
    }

}

