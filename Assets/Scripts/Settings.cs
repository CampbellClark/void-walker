using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Settings : MonoBehaviour
{

    bool isFullScreen;
    
    



    public void FullScreen()
    {
        if (!isFullScreen)
        {
            Screen.fullScreen = Screen.fullScreen;
            isFullScreen = true;
            Debug.Log("full");
        }
        
    }


    public void Tabbed()
    {
        if (isFullScreen)
        {
            Screen.fullScreen = !Screen.fullScreen;
            Debug.Log("tab");

            isFullScreen = false;
        }
    }
}
