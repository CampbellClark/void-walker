using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int dieTime, damage;
    public GameObject diePeffect;
    Player player;
    
    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<Player>();
        StartCoroutine(CountDownTimer());
    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        Die();
    }

    IEnumerator CountDownTimer()
    {
        yield return new WaitForSeconds(dieTime);

        Die();
    }
    void Die()
    {
        Destroy(gameObject);
    }

   
    
}
