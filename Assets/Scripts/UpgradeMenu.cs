using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class UpgradeMenu : MonoBehaviour
{
    
    public GameObject upgradeMenu;
    // Start is called before the first frame update

    

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.U) && Time.timeScale == 1)
        {
            OpenUpgrade();
        }
        

        else if(Input.GetKeyDown(KeyCode.U) && Time.timeScale == 0)
        {
            CloseUpgrade();
        }

        
    }

    public void OpenUpgrade()
    {
        upgradeMenu.SetActive(true);
        Time.timeScale = 0;
        Debug.Log("open");

    }

    public void CloseUpgrade()
    {
        Debug.Log("Closed");
        upgradeMenu.SetActive(false);
        Time.timeScale = 1;
    }
}
