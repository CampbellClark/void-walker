using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerMenu : MonoBehaviour
{
    public GameObject buttons;
    public GameObject settings;
   public void PlayGame()
    {
        SceneManager.LoadScene("Tutorial");
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }


    public void Settings()
    {
        buttons.SetActive(false);
        settings.SetActive(true);

    }

    public void ReturnToButtons()
    {
        settings.SetActive(false);
        buttons.SetActive(true);
    }

}
