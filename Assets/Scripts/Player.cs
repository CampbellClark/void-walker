using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{

	[System.Serializable]
	public class PlayerStats
	{
		public int maxHealth = 100;

		private int _curHealth;

		public int lives = 3;
		public int curHealth
        {
            get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
        }

		public void Init()
        {
			curHealth = maxHealth;
        }
	}

	public PlayerStats stats = new PlayerStats();

	public int fallBoundary = -20;

	public Text youDied;
	public bool isBossLeve;

	[SerializeField]
	private StatusIndicator statusIndicator;

	private void Start()
	{
		stats.Init();
		if (statusIndicator == null)
		{
			Debug.LogError("error");

		}
        else
        {
			statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
		}
    }

    void Update()
	{
		if (transform.position.y <= fallBoundary)
			DamagePlayer(9999999);

		//if(stats.lives <= 0)
        //{
		//	youDied.gameObject.SetActive(true);
        //}//
		
	}

	public void DamagePlayer(int damage)
	{
		
		stats.curHealth -= damage;
		

		if (stats.curHealth <= 0)
		{
			FindObjectOfType<AudioManager>().Play("PlayerDeath");

			GameMaster.KillPlayer(this);
		}
		if(isBossLeve && stats.curHealth <= 0)
        {
			FindObjectOfType<AudioManager>().Play("PlayerDeath");

			SceneManager.LoadScene("LavaPlanet");
        }
		FindObjectOfType<AudioManager>().Play("PlayerDmg");
		statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
	}

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
			Destroy(other.gameObject);
        }

		if (other.gameObject.tag == "Spike")
		{
			DamagePlayer(999999);
		}
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
			DamagePlayer(15);
        }
        
    }


	
}
