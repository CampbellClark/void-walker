using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public bool isTutorial;
    public bool isLevel1;
    public bool isLevel2;
    public bool isLevel3;
    public bool islevel4;

    public Animator anim;

   
    private void Start()
    {
        FindObjectOfType<AudioManager>().Play("ChillMoon");

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isLevel1)
        {
            SceneManager.LoadScene("JunglePlanet");
        }
        if (isLevel2)
        {
            SceneManager.LoadScene("LavaPlanet");
        }
        if (isTutorial)
        {
            SceneManager.LoadScene("SampleScene");
        }

        if (islevel4)
        {
            SceneManager.LoadScene("Abyss");
        }
    }
  



   
}
