using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public bool spawner1, spawner2;
      
    public GameObject enemy1;
    public GameObject enemy2;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            if (spawner1)
            {
                enemy1.SetActive(true);
                return;

            }
            if (spawner2)
            {
                enemy2.SetActive(true);
                return;
            }
        }
       
    }
}
