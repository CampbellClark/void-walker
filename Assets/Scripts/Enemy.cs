using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class Enemy : MonoBehaviour
{

	[System.Serializable]
	public class EnemyStats
	{
		public int maxHealth = 100;
	

		private int _curHealth;
		public int curHealth
        {
            get { return _curHealth; }
            set { _curHealth = Mathf.Clamp(value, 0, maxHealth); }
        }

		public int damage = 10;

		public void Init()
        {
			curHealth = maxHealth;
        }
	}


	LootDrop lootDrop;
	public bool isBossScript;
	public EnemyStats stats = new EnemyStats();

	[Header("Optional: ")]
	[SerializeField]
	private StatusIndicator statusIndicator;

	Rigidbody2D rb;

	// Cam shake handler

	CameraShake camShake;
	public float camShakeAmt = 0.1f;
	public float camShakeLen = 0.05f;
	public SpriteRenderer spriteRenderer;
	public aiPatrol aiPatrol;

	public Animator anim;
	private void Start()
    {
		rb = GetComponent<Rigidbody2D>();
		camShake = GameMaster.gm.GetComponent<CameraShake>();
		stats.Init();
		if(statusIndicator != null)
        {
			statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
        }
    }
    public void DamageEnemy(int damage)
	{
		
		stats.curHealth -= damage;
		if (stats.curHealth <= 0 && !isBossScript)
		{
			FindObjectOfType<AudioManager>().Play("GrenadeExplosion");

			GameMaster.KillEnemy(this);
		}
		else if(isBossScript == true && stats.curHealth <= 0)
		{
			FindObjectOfType<AudioManager>().Play("bossDeath");
			
			transform.position = new Vector3 (100, 20, 20);

			StartCoroutine(bossDeathCounter());
			
			

		}
		

		if (statusIndicator != null)
		{
			statusIndicator.SetHealth(stats.curHealth, stats.maxHealth);
		}
		

	}

    private void OnCollisionEnter2D(Collision2D _colInfo)
    {
		Vector2 impulse = new Vector2(20, 7);

		Player _player = _colInfo.collider.GetComponent<Player>();
		if(_player != null && !isBossScript)
        {
			_player.DamagePlayer(stats.damage);
			GetComponent<Rigidbody2D>().AddForce(impulse, ForceMode2D.Impulse);
		}
    }



	IEnumerator bossDeathCounter()
    {

		yield return new WaitForSeconds(5);
		GameMaster.KillEnemy(this);
		

		SceneManager.LoadScene("Credits");
	}



}
