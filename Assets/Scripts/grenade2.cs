using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class grenade2 : MonoBehaviour
{

    public float delay = 3f;
    public int grenadeDamage = 100;
    public float radius = 40f;
    public GameObject grenade;

    public Transform attackPoint;
    public float attackRange = 5f;
    public LayerMask enemyLayers;
    Enemy enemy;

    public GameObject explosionEffect;

    float countdown;
    bool hasExploded = false;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0 && hasExploded == false)
        {
            Explode();
            hasExploded = true;
            
        }
    }

    void Explode()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);

        hasExploded = true;
        Destroy(gameObject);

        //Debug.Log(hasExploded);

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, radius, enemyLayers);
        foreach (Collider2D enemy in hitEnemies)
        {
            enemy.GetComponent<Enemy>().DamageEnemy(grenadeDamage);
            
            hasExploded = true;
        }
        Instantiate(explosionEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
