using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienDotAi : MonoBehaviour
{
    public float speed;
    public float range;
    public float shootSpeed;
    private float distToPlayer;
    private bool canShoot = true;
    public bool movingLeft = true;


    public float timeBetweenShots;

    public Animator anim;
    public Transform groundDetection;
    private bool searchingForPlayer = false;

    public Transform player;
    

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            if (!searchingForPlayer)
            {
                searchingForPlayer = true;
                StartCoroutine(SearchForPlayer());
            }
            return;
        }

        transform.Translate(Vector2.left * speed * Time.deltaTime);

        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, 2f);
        if (groundInfo.collider == false)
        {
            if (movingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingLeft = false;
                anim.SetBool("movingLeft", false);

            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = true;
                anim.SetBool("movingLeft", true);

            }
        }
        distToPlayer = Vector2.Distance(transform.position, player.position);

        if (distToPlayer <= range)
        {
            if (player.position.x > transform.position.x && transform.localScale.x < 0 || player.position.x < transform.position.x && transform.localScale.x > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                movingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                movingLeft = true;
            }
           
        }
    }
    IEnumerator SearchForPlayer()
    {
        GameObject sResult = GameObject.FindGameObjectWithTag("Player");
        if (sResult == null)
        {
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(SearchForPlayer());
        }
        else
        {
            searchingForPlayer = false;
            player = sResult.transform;

            yield break;
        }
    }
}
